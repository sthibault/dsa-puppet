class linux {
	include ferm
	include ferm::per_host
	include entropykey
	include rng_tools
	if $::hostname in [fasolo, klecker, mirror-conova, storace, sallinen] {
		$blacklist_acpi_power_meter = true
	}
	if $blacklist_acpi_power_meter {
		file { '/etc/modprobe.d/hwmon.conf':
			content => "blacklist acpi_power_meter"
		}
	}
}
