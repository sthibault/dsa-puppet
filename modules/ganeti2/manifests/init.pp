# = Class: ganeti2
#
# Standard ganeti2 config debian.org hosts
#
# == Sample Usage:
#
#   include ganeti2
#
class ganeti2 {

	include ganeti2::params
	include ganeti2::firewall

	$drbd = $ganeti2::params::drbd

	package { 'ganeti':
		ensure => installed
	}

	site::linux_module { 'tun': }

	file { '/etc/cron.hourly/puppet-cleanup-watcher-pause-file':
		source => 'puppet:///modules/ganeti2/cleanup-watcher-pause-file',
		mode    => '0555',
	}

	if $::debarchitecture == 'arm64' {
		file { '/usr/local/bin/qemu-system-aarch64-wrapper':
			source => 'puppet:///modules/ganeti2/qemu-system-aarch64-wrapper',
			mode   => '0555',
		}
	}
	if $::cluster == 'ganeti.bm.debian.org' {
		file { '/usr/local/sbin/crazy-multipath-restart':
			source => 'puppet:///modules/ganeti2/crazy-multipath-restart',
			mode   => '0555',
		}
		file { '/etc/cron.d/puppet-crazy-multipath-restart': ensure => absent, }
		concat::fragment { 'dsa-puppet-stuff--multipath-restart':
			target => '/etc/cron.d/dsa-puppet-stuff',
			content  => @("EOF"),
					*/15 * * * * root /usr/local/sbin/crazy-multipath-restart
					| EOF
		}
	}
	file { '/usr/local/sbin/ganeti-reboot-cluster':
		source => 'puppet:///modules/ganeti2/ganeti-reboot-cluster',
		mode   => '0555',
	}

	package { ['python-dbus', 'systemd-container']: ensure => installed }
	file { '/usr/local/sbin/ganeti-machined-register-instances':
		source => 'puppet:///modules/ganeti2/ganeti-machined-register-instances',
		mode   => '0555',
	}
	file { [
		'/etc/ganeti/hooks',
		'/etc/ganeti/hooks/instance-reboot-post.d',
		'/etc/ganeti/hooks/instance-migrate-post.d',
		'/etc/ganeti/hooks/instance-start-post.d',
		'/etc/ganeti/hooks/instance-failover-post.d',
		'/etc/ganeti/hooks/instance-add-post.d',
		]:
		ensure => directory,
	}
	file { [
		'/etc/ganeti/hooks/instance-reboot-post.d/00-ganeti-machined-register-instances',
		'/etc/ganeti/hooks/instance-migrate-post.d/00-ganeti-machined-register-instances',
		'/etc/ganeti/hooks/instance-start-post.d/00-ganeti-machined-register-instances',
		'/etc/ganeti/hooks/instance-failover-post.d/00-ganeti-machined-register-instances',
		'/etc/ganeti/hooks/instance-add-post.d/00-ganeti-machined-register-instances',
		]:
		ensure => link,
		target => '/usr/local/sbin/ganeti-machined-register-instances',
	}
}
