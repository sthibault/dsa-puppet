class fail2ban::postfix inherits fail2ban {
	file { '/etc/fail2ban/filter.d/dsa-postfix.conf':
		source => 'puppet:///modules/fail2ban/filter/dsa-postfix.conf',
		notify  => Service['fail2ban'],
	}
	file { '/etc/fail2ban/jail.d/dsa-postfix.conf':
		source => 'puppet:///modules/fail2ban/jail/dsa-postfix.conf',
		notify  => Service['fail2ban'],
	}
}
