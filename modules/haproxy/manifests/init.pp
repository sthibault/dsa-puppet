class haproxy {
	package { 'haproxy':
		ensure => installed,
	}
	service { 'haproxy':
		ensure => running,
		require => Package['haproxy'],
	}

	file { '/usr/local/bin/munin-haproxyng':
		mode => '0555',
		source => "puppet:///modules/haproxy/monitoring-munin-haproxy/haproxyng",
	}
	package { 'libswitch-perl':
		ensure => installed,
	}
	file { '/etc/munin/plugin-conf.d/puppet-haproxyng':
		source => "puppet:///modules/haproxy/munin-haproxyng.conf",
	}
	munin::check { "haproxyng":
		script => "../../../local/bin/munin-haproxyng",
	}
}
