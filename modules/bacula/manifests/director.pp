class bacula::director inherits bacula {

	package { ['bacula-director-pgsql', 'bacula-common', 'bacula-common-pgsql']:
		ensure => installed
	}

	service { 'bacula-director':
		ensure    => running,
		enable    => true,
		hasstatus => true,
		require   => Package['bacula-director-pgsql']
	}
	systemd::override { 'bacula-director':
		content => @(EOT)
			[Unit]
			After=unbound.service
			| EOT
	}

	exec { 'bacula-director reload':
		path        => '/usr/bin:/usr/sbin:/bin:/sbin',
		command     => 'service bacula-director reload',
		refreshonly => true,
	}

	file { '/etc/bacula/conf.d':
		ensure  => directory,
		mode    => '0755',
		group   => bacula,
		purge   => true,
		force   => true,
		recurse => true,
		source  => 'puppet:///files/empty/',
		notify  => Exec['bacula-director reload']
	}

	file { '/etc/bacula/bacula-dir.conf':
		content => template('bacula/bacula-dir.conf.erb'),
		mode    => '0440',
		group   => bacula,
		require => Package['bacula-director-pgsql'],
		notify  => Exec['bacula-director reload']
	}

	file { '/etc/bacula/conf.d/empty.conf':
		content => '',
		mode    => '0440',
		group   => bacula,
		require => Package['bacula-director-pgsql'],
		notify  => Exec['bacula-director reload']
	}

	Bacula::Node<<| |>>

	package { 'bacula-console':
		ensure => installed;
	}

	file { '/etc/bacula/bconsole.conf':
		content => template('bacula/bconsole.conf.erb'),
		mode    => '0640',
		group   => bacula,
		require => Package['bacula-console']
	}

	package { 'python3-psycopg2': ensure => installed }
	file { '/etc/bacula/scripts/volume-purge-action':
		mode    => '0555',
		source  => 'puppet:///modules/bacula/volume-purge-action',
		;
	}
	file { '/etc/bacula/scripts/volumes-delete-old':
		mode    => '0555',
		source  => 'puppet:///modules/bacula/volumes-delete-old',
		;
	}
	file { '/etc/bacula/storages-list.d':
		ensure  => directory,
		mode    => '0755',
		group   => bacula,
		purge   => true,
		force   => true,
		recurse => true,
		source  => 'puppet:///files/empty/',
	}
	file { '/usr/local/sbin/dsa-bacula-scheduler':
		source  => 'puppet:///modules/bacula/dsa-bacula-scheduler',
		mode    => '0555',
	}

	file { "/etc/cron.d/puppet-bacula-stuff": ensure => absent, }
	concat::fragment { 'dsa-puppet-stuff--bacula-director':
		target => '/etc/cron.d/dsa-puppet-stuff',
		content  => @(EOF)
			@daily root chronic /etc/bacula/scripts/volume-purge-action -v
			@daily root chronic /etc/bacula/scripts/volumes-delete-old -v
			*/3 * * * * root sleep $(( $RANDOM \% 60 )); flock -w 0 -e /usr/local/sbin/dsa-bacula-scheduler /usr/local/sbin/dsa-bacula-scheduler
			| EOF
	}

	concat { $bacula::bacula_dsa_client_list:
	}
	concat::fragment { 'bacula-dsa-client-list::header' :
		target => $bacula::bacula_dsa_client_list,
		content  => "",
		order  => '00',
	}
	Concat::Fragment <<| tag == $bacula::tag_bacula_dsa_client_list |>>
}
