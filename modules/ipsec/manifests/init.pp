class ipsec {
	$ipsec_config = @(EOF)
		--- 

		storace.debian.org:
		  address: 93.94.130.161

		fasolo.debian.org:
		  address: 138.16.160.17

		| EOF

	package { [
		'strongswan',
		'libstrongswan-standard-plugins'
		]:
		ensure => installed
	}

	service { 'ipsec':
		ensure => running,
	}

	file { '/etc/ipsec.conf':
		content  => template("ipsec/ipsec.conf.erb"),
		notify  => Service['ipsec'],
	}
	file { '/etc/ipsec.secrets':
		mode => '0400',
		content  => template("ipsec/ipsec.secrets.erb"),
		notify  => Service['ipsec'],
	}

	file { '/etc/ipsec.conf.d':
		mode => '0755',
		ensure => 'directory',
	}
	file { '/etc/ipsec.secrets.d':
		ensure => 'directory',
		mode => '0700',
	}

	file { '/etc/ipsec.conf.d/00-default.conf':
		content  => template("ipsec/ipsec.conf-00-default.conf.erb"),
		notify  => Service['ipsec'],
	}

	file { '/etc/ipsec.conf.d/10-puppet-peers.conf':
		content => template("ipsec/ipsec.conf-10-puppet-peers.conf.erb"),
		notify  => Service['ipsec'],
	}
	file { '/etc/ipsec.secrets.d/10-puppet-peers.secrets':
		mode => '0400',
		content => template("ipsec/ipsec.secrets-10-puppet-peers.secrets.erb"),
		notify  => Service['ipsec'],
	}

	file {
		"/etc/ferm/dsa.d/10-ipsec":
			mode    => '0400',
			content => template("ipsec/ferm.erb"),
			notify  => Service['ferm'],
	}
}
