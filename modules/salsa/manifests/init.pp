#
class salsa inherits salsa::params {

	# anchor things in correct order
	anchor { 'salsa::begin': } ->
	class { '::salsa::mail': } ->
	class { '::salsa::redis': } ->
	class { '::salsa::packages': } ->
	class { '::salsa::database': } ->
	class { '::salsa::web': } ->
	anchor { 'salsa::end': }

	# userdir-ldap users get their home in /home
	file { "/home/${salsa::user}":
		ensure => link,
		target => $salsa::home,
	}
	file { $salsa::home:
		ensure => directory,
		mode   => '0755',
		owner  => $salsa::user,
		group  => $salsa::group,
	}
	file { "/home/${salsa::registry_user}":
		ensure => link,
		target => $salsa::registry_user_home,
	}
	file { $salsa::registry_user_home:
		ensure => directory,
		mode   => '0755',
		owner  => $salsa::registry_user,
		group  => $salsa::registry_user,
	}
	file { "/home/${salsa::signup_user}":
		ensure => link,
		target => $salsa::signup_user_home,
	}
	file { $salsa::signup_user_home:
		ensure => directory,
		mode   => '0755',
		owner  => $salsa::signup_user,
		group  => $salsa::signup_user,
	}
	file { "/home/${salsa::webhook_user}":
		ensure => link,
		target => $salsa::webhook_user_home,
	}
	file { $salsa::webhook_user_home:
		ensure => directory,
		mode   => '0755',
		owner  => $salsa::webhook_user,
		group  => $salsa::webhook_user,
	}
	file { "/home/${salsa::pages_user}":
		ensure => link,
		target => $salsa::pages_user_home,
	}
	file { $salsa::pages_user_home:
		ensure => directory,
		mode   => '0755',
		owner  => $salsa::pages_user,
		group  => $salsa::pages_user,
	}


	file { "${salsa::home}/.credentials.yaml":
		mode => '0400',
		owner  => $salsa::user,
		group  => $salsa::group,
		content  => @("EOF"),
				---
				# This file is maintained by puppet.
				# base secret that gitlab encrypts the DB with
				secret: "${salsa::secret}"
				database:
				  name: "${salsa::db_name}"
				  role: "${salsa::db_role}"
				  password: "${salsa::db_password}"
				mail:
				  username: "${salsa::mail_username}"
				  password: "${salsa::mail_password}"
				| EOF
	}
	file { "${salsa::home}/.credentials-manual.yaml":
		mode => '0400',
		owner  => $salsa::user,
		group  => $salsa::group,
		content  => @("EOF"),
				---
				# This file was put in place by puppet, but it won't overwrite it.
				# Please fill in from dsa-passwords/services-salsa
				# mastersecret: "swordfish"
				| EOF
		replace => false,
	}
	file { "/var/lib/systemd/linger/${salsa::user}":
		ensure => present,
	}
	file { "/var/lib/systemd/linger/${salsa::registry_user}":
		ensure => present,
	}
	file { "/var/lib/systemd/linger/${salsa::signup_user}":
		ensure => present,
	}
	file { "/var/lib/systemd/linger/${salsa::webhook_user}":
		ensure => present,
	}
	file { "/var/lib/systemd/linger/${salsa::pages_user}":
		ensure => present,
	}
	file { "/etc/ssh/userkeys/${salsa::user}":
		ensure => link,
		target => "${salsa::home}/.ssh/authorized_keys",
	}
	# pages
	file { "/etc/network/interfaces.d/pages.debian.net.conf":
		content  => @("EOF"),
				iface eth0 inet static
				    address 209.87.16.45/24
				iface eth0 inet6 static
				    address 2607:f8f0:614:1::1274:45/64
				    preferred-lifetime 0
				| EOF
		notify => Exec['service networking reload'],
	}
	exec { 'service networking reload':
		refreshonly => true,
	}
}
