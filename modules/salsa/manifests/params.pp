#
class salsa::params {
	$servicename = "salsa.debian.org"

	$user = "git"
	$group = "git"
	$home = "/srv/${servicename}"
	$registry_user = "salsa-registry"
	$registry_user_home = "/srv/registry.${servicename}"
	$signup_user = "salsa-signup"
	$signup_user_home = "/srv/signup.${servicename}"
	$webhook_user = "salsa-webhook"
	$webhook_user_home = "/srv/webhook.${servicename}"
	$pages_user = "salsa-pages"
	$pages_user_home = "/srv/pages.debian.net"

	$db_name = "salsa"
	$db_role = "salsa"
	$db_password = hkdf('/etc/puppet/secret', "postgresql-${::hostname}-${servicename}-${db_role}")

	$mail_username = "gitlab@${servicename}"
	$mail_password = hkdf('/etc/puppet/secret', "mail-imap-dovecot-${::hostname}-${servicename}-${mail_username}")

	$secret = hkdf('/etc/puppet/secret', "salsa-${::hostname}-base-secret")
}
