#
class salsa::packages inherits salsa {
	$requirements = [
		'build-essential',
		'bundler',
		'checkinstall',
		'cmake',
		'curl',
		'golang',
		'libcurl4-openssl-dev',
		'libffi-dev',
		'libgdbm-dev',
		'libicu-dev',
		'libncurses5-dev',
		'libre2-dev',
		'libreadline-dev',
		'libssl-dev',
		'libxml2-dev',
		'libxslt1-dev',
		'libyaml-dev',
		'logrotate',
		'node-mkdirp',
		'node-semver',
		'nodejs',
		'nodejs-legacy',
		'pkg-config',
		'python-docutils',
		'python-requests',
		'python3-requests',
		'ruby-dev',
		'ruby-sinatra',
		'ruby-sinatra-contrib',
		'ruby-mail',
		'ruby-soap4r',
		'thin',
		'libpq-dev',
		'zlib1g-dev'
	]

	ensure_packages($requirements, { ensure => 'installed' })

	$mgmt_requirements = [
		'ansible',
		'python-hkdf',
		'ruby-ldap',
	]

	ensure_packages($mgmt_requirements, { ensure => 'installed' })

	$registrationapp_requirements = [
		'libapache2-mod-wsgi-py3',
		'libjs-bootstrap',
		'python3',
		'python3-flask',
		'python3-flaskext.wtf',
		'python3-oauthlib',
	]

	ensure_packages($registrationapp_requirements, { ensure => 'installed' })
}
