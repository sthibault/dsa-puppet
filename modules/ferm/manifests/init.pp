# = Class: ferm
#
# This class installs ferm and sets up rules
#
# == Sample Usage:
#
#   include ferm
#
class ferm {
	# realize (i.e. enable) all @ferm::rule virtual resources
	Ferm::Rule <| |>
	Ferm::Conf <| |>

	File { mode => '0400' }

	package { 'ferm':
		ensure => installed
	}
	package { 'ulogd2':
		ensure => installed
	}
	package { 'ulogd':
		# Remove instead of purge ulogd because it deletes log files on purge.
		ensure => absent
	}

	service { 'ferm':
		hasstatus   => false,
		status      => '/bin/true',
	}

	$munin_ips = getfromhash($site::nodeinfo, 'misc', 'v4addrs')
		.map |$addr| { "ip_${addr}" }

	munin::check { $munin_ips: script => 'ip_', }

	$munin6_ips = getfromhash($site::nodeinfo, 'misc', 'v6addrs')
		.map |$addr| { "ip_${addr}" }
	munin::ipv6check { $munin6_ips: }

	file { '/etc/ferm':
		ensure  => directory,
		notify  => Service['ferm'],
		require => Package['ferm'],
		mode    => '0755'
	}
	file { '/etc/ferm/dsa.d':
		ensure => directory,
		mode   => '0555',
		purge   => true,
		force   => true,
		recurse => true,
		source  => 'puppet:///files/empty/',
	}
	file { '/etc/ferm/conf.d':
		ensure => directory,
		mode   => '0555',
		purge   => true,
		force   => true,
		recurse => true,
		source  => 'puppet:///files/empty/',
	}
	file { '/etc/default/ferm':
		source  => 'puppet:///modules/ferm/ferm.default',
		require => Package['ferm'],
		notify  => Service['ferm'],
		mode    => '0444',
	}
	file { '/etc/ferm/ferm.conf':
		content => template('ferm/ferm.conf.erb'),
		notify  => Service['ferm'],
	}
	file { '/etc/ferm/conf.d/00-init.conf':
		content => template('ferm/00-init.conf.erb'),
		notify  => Service['ferm'],
	}
	file { '/etc/ferm/conf.d/me.conf':
		content => template('ferm/me.conf.erb'),
		notify  => Service['ferm'],
	}
	file { '/etc/ferm/conf.d/defs.conf':
		content => template('ferm/defs.conf.erb'),
		notify  => Service['ferm'],
	}

	file { '/etc/ferm/conf.d/50-munin-interfaces.conf':
		content => template('ferm/conf.d-munin-interfaces.conf.erb'),
		notify  => Service['ferm'],
	}
	@ferm::rule { 'dsa-munin-interfaces-in':
		prio        => '001',
		description => 'munin accounting',
		chain       => 'INPUT',
		domain      => '(ip ip6)',
		rule        => 'daddr ($MUNIN_IPS) NOP'
	}
	@ferm::rule { 'dsa-munin-interfaces-out':
		prio        => '001',
		description => 'munin accounting',
		chain       => 'OUTPUT',
		domain      => '(ip ip6)',
		rule        => 'saddr ($MUNIN_IPS) NOP'
	}

	file { '/etc/ferm/dsa.d/010-base.conf':
		content => template('ferm/dsa.d-010-base.conf.erb'),
		notify  => Service['ferm'],
	}

	augeas { 'logrotate_ulogd2':
		context => '/files/etc/logrotate.d/ulogd2',
		changes => [
			'set rule/schedule daily',
			'set rule/delaycompress delaycompress',
			'set rule/rotate 10',
			'set rule/ifempty notifempty',
		],
	}
	file { '/etc/logrotate.d/ulogd':
		ensure  => absent,
	}
	file { '/etc/logrotate.d/ulogd.dpkg-bak':
		ensure  => absent,
	}
	file { '/etc/logrotate.d/ulogd.dpkg-dist':
		ensure  => absent,
	}

}
