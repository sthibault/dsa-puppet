class roles::tracker {
	package { 'libapache2-mod-wsgi-py3': ensure => installed, }
	apache2::module { 'wsgi': require => Package['libapache2-mod-wsgi-py3'] }
	ssl::service { 'tracker.debian.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}
	onion::service { 'tracker.debian.org': port => 80, target_address => 'tracker.debian.org', target_port => 80, direct => true }
}
