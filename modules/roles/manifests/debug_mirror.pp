class roles::debug_mirror {
	include roles::archvsync_base

	$mirror_basedir_prefix = hiera('role_config__mirrors.mirror_basedir_prefix')
	$archive_root = "${mirror_basedir_prefix}debian-debug"

	$listen_addresses = hiera('roles.debug_mirror')
		.dig($::fqdn, 'listen-addresses')
		.then |$a| { $a + [ '127.0.0.1:80', '[::1]:80' ] }
		.lest || { ['*:80'] }

	$vhost_listen = join($listen_addresses, ' ')
	$onion_v4_addr = hiera("roles.debug_mirror", {})
		.dig($::fqdn, 'onion_v4_address')

	apache2::site { '010-debug.mirrors.debian.org':
		site   => 'debug.mirrors.debian.org',
		content => template('roles/apache-debug.mirrors.debian.org.erb'),
	}

	if has_role('debug_mirror_onion') {
		if ! $onion_v4_addr {
			fail("Do not have an onion_v4_addr set for $::hostname.")
		}

		onion::service { 'debug.mirrors.debian.org':
			port => 80,
			target_port => 80,
			target_address => $onion_v4_addr,
		}
	}

	$hosts_to_check = hiera('roles.debug_mirror', {})
		.map |$h| { $h[1]['service-hostname'] }
	roles::mirror_health { 'debug':
		check_hosts	=> $hosts_to_check,
		check_service	=> 'debug',
		url		=> 'http://debug.backend.mirrors.debian.org/debian-debug/dists/sid-debug/Release',
		health_url	=> 'http://debug.backend.mirrors.debian.org/_health',
	}
}
