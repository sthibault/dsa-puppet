class roles::debconf_wafer {
	include apache2::ssl
	include apache2::expires

	package { 'libapache2-mod-wsgi-py3': ensure => installed, }
	apache2::module { 'wsgi': require => Package['libapache2-mod-wsgi-py3'] }

	ssl::service { 'wafertest.debconf.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}
	apache2::site { '010-wafertest.debconf.org':
		site    => 'wafertest.debconf.org',
		source => 'puppet:///modules/roles/debconf_wafer/wafertest.debconf.org',
	}

	ssl::service { 'debconf18.debconf.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}
	apache2::site { '010-debconf18.debconf.org':
		site    => 'debconf18.debconf.org',
		source => 'puppet:///modules/roles/debconf_wafer/debconf18.debconf.org',
	}

	ssl::service { 'debconf19.debconf.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}
	apache2::site { '010-debconf19.debconf.org':
		site    => 'debconf19.debconf.org',
		source => 'puppet:///modules/roles/debconf_wafer/debconf19.debconf.org',
	}
}

