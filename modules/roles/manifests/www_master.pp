class roles::www_master {
	ssl::service { 'www-master.debian.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}

	$vhost_listen = '*:80'
	$vhost_listen_443 = '*:443'
	$redirect_vhosts = false
	$wwwdo_server_name = 'www-staging.debian.org'
	$wwwdo_document_root = '/srv/www.debian.org/www'
	apache2::site { '005-www-staging.debian.org':
		site    => 'www-staging.debian.org',
		content => template('roles/apache-www.debian.org.erb'),
	}

	ssl::service { 'www-staging.debian.org' :
		notify => Exec['service apache2 reload'],
		key    => true,
	}
}
