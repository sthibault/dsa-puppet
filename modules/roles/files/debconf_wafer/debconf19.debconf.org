# do not modify - this file is maintained via puppet

AddType application/font-woff2 .woff2

Use common-debian-service-https-redirect * debconf19.debconf.org

WSGIDaemonProcess debconf19 \
  processes=3 threads=2 \
  user=www-data group=debconf-web maximum-requests=750 umask=0007 display-name=wsgi-debconf19.debconf.org \
  python-path=/srv/debconf-web/debconf19.debconf.org/dc19.dc.o/:/srv/debconf-web/debconf19.debconf.org/dc19.dc.o/ve/lib/python3.5/site-packages/

<VirtualHost *:443>
  ServerAdmin admin@debconf.org
  ServerName debconf19.debconf.org

  ErrorLog  /var/log/apache2/debconf19.debconf.org-error.log
  CustomLog /var/log/apache2/debconf19.debconf.org-access.log combined

  Use common-debian-service-ssl debconf19.debconf.org
  Use common-ssl-HSTS

  Header always set Referrer-Policy "same-origin"
  Header always set X-Content-Type-Options nosniff
  Header always set X-XSS-Protection "1; mode=block"
#  Header always set Access-Control-Allow-Origin: "*"

  # Debian SSO
  SSLCACertificateFile /var/lib/dsa/sso/ca.crt
  SSLCARevocationCheck chain
  SSLCARevocationFile /var/lib/dsa/sso/ca.crl

  WSGIProcessGroup debconf19
  WSGIScriptAlias / /srv/debconf-web/debconf19.debconf.org/dc19.dc.o/wsgi.py
  WSGIPassAuthorization On

  <Directory /srv/debconf-web/debconf19.debconf.org/dc19.dc.o>
    <Files wsgi.py>
      Require all granted
    </Files>
  </Directory>

  Alias /static/ /srv/debconf-web/debconf19.debconf.org/dc19.dc.o/localstatic/
  Alias /favicon.ico /srv/debconf-web/debconf19.debconf.org/dc19.dc.o/localstatic/img/favicon/favicon.ico
  <Directory /srv/debconf-web/debconf19.debconf.org/dc19.dc.o/localstatic/>
    Require all granted

    # A little hacky, but it means we won't accidentally catch non-hashed filenames
    <FilesMatch ".*\.[0-9a-f]{12}\.[a-z0-9]{2,5}$">
      ExpiresActive on
      ExpiresDefault "access plus 1 year"
    </FilesMatch>
  </Directory>

  Alias /media/ /srv/debconf-web/debconf19.debconf.org/dc19.dc.o/media/
  <Directory /srv/debconf-web/debconf19.debconf.org/dc19.dc.o/media/>
    Require all granted
  </Directory>

  <Location /accounts/debian-login>
    SSLOptions +StdEnvVars
    # Allow access if one does not have a valid certificate
    SSLVerifyClient optional
  </Location>
</VirtualHost>

# vim: set ft=apache:
